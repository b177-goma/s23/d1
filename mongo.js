//select a database
use <database name>

//when creating a new database via the command line, the use command can be entered with a name of a database that does not yet exist. Once a new record is inserted into that database, the database will be created.

//database = filing cabinet
//collection = drawer
//document/record = folder inside a drawer
//sub-documents (optional) = other files
//fields = file content

/*
e.g. document with no sub-documents:
	{
		name: "Jino Yumul",
		age: 33,
		occupation: "Instructor"
	}
e.g. document with sub-documents:
	{
		name: "Jino Yumul",
		age: 33,
		occupation: "Instructor",
		address: {
			street: "123 Street St",
			city: "Makati"
			country: "Philippines"
		}
	}
/*

/*
Embedded vs. Referenced data:

Referenced data:

Users:
	{
		id: 298
		name: "Jino Yumul",
		age: 33,
		occupation: "Instructor"
	}

Orders:
	{
		products: [
			{
				name: "New pillow",
				price: 300
			}
		],
		userId: 298
	}

Embedded data:

Users:
	{
		id: 298
		name: "Jino Yumul",
		age: 33,
		occupation: "Instructor",
		orders: [
			{
				products: [
					{
						name: "New pillow",
						price: 300
					}
				]
			}
		]
	}

*/

//Insert One Document (Create)

db.users.insert({
	firstName: "Jane",
	lastName: "Doe"
	age: 21,
	contact: {
		phone: "123456789",
		email: "janedoe@mail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
})

//if inserting/creating a new document within a collection that does not yet exist, MongoDB will automatically create that collection

//insert many

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "123456789",
			email: "stephenhawking@mail.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "123456789",
			email: "neilarmstrong@mail.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	}
])
